package main

import (
	"reflect"
	"testing"
)

func TestConvertResultJSONToSlice(t *testing.T) {
	// Test case 1: Valid JSON input
	jsonData := []byte(`[
		{"id": 1, "name": "Project 1"},
		{"id": 2, "name": "Project 2"}
	]`)

	expectedResult := []map[string]interface{}{
		{"id": 1.0, "name": "Project 1"},
		{"id": 2.0, "name": "Project 2"},
	}

	result := convertResultJSONToSlice(jsonData)

	if !reflect.DeepEqual(result, expectedResult) {
		t.Errorf("Test case 1 failed: Expected %v, got %v", expectedResult, result)
	}

	// Test case 2: Invalid JSON input
	invalidJSONData := []byte(`"invalid json"`)

	result = convertResultJSONToSlice(invalidJSONData)

	if result != nil {
		t.Errorf("Test case 2 failed: Expected nil, got %v", result)
	}

}
