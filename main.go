package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
)

const (
	gitLabAPIEndpoint = "https://gitlab.com/api/v4"
	defaultUserID = 8148677
)

func main() {
	userID := getUserID()
	if userID == -1 {
		fmt.Println("Invalid user ID")
		return
	}

	projectIDs := getPublicProjectIDs(userID)
	for _, projectID := range projectIDs {
		projectLangs := getProjectLanguages(projectID)
		fmt.Printf("Response projectLangs for Project ID %d: %v\n", projectID, projectLangs)
	}
}

func getUserID() int {
	if len(os.Args) > 1 {
		value, err := strconv.Atoi(os.Args[1])
		if err != nil {
			fmt.Println(err)
			return defaultUserID
		}
		return value
	}
	return defaultUserID
}

func getProjectLanguages(projectID int) map[string]float64 {
	url := fmt.Sprintf("%s/projects/%d/languages", gitLabAPIEndpoint, projectID)
	responseBody := performHTTPRequest(url)
	return convertResultJSONToMap(responseBody)
}

func getPublicProjectIDs(userID int) []int {
	url := fmt.Sprintf("%s/users/%d/projects", gitLabAPIEndpoint, userID)
	responseBody := performHTTPRequest(url)

	projects := convertResultJSONToSlice(responseBody)
	return extractProjectIDs(projects)
}

func performHTTPRequest(url string) []byte {
	response, err := http.Get(url)
	if err != nil {
		fmt.Println("Error:", err)
		return []byte{}
	}
	defer response.Body.Close()

	result, err := io.ReadAll(response.Body)
	if err != nil {
		fmt.Println("Error reading response body:", err)
		return []byte{}
	}

	return result
}

func convertResultJSONToMap(resultJSON []byte) map[string]float64 {
	var resultMap map[string]float64
	err := json.Unmarshal(resultJSON, &resultMap)
	if err != nil {
		fmt.Println("Error unmarshaling JSON:", err)
		return nil
	}
	return resultMap
}

func convertResultJSONToSlice(resultJSON []byte) []map[string]interface{} {
	var resultMap []map[string]interface{}
	err := json.Unmarshal(resultJSON, &resultMap)
	if err != nil {
		fmt.Println("Error unmarshaling JSON:", err)
		return nil
	}
	return resultMap
}

func extractProjectIDs(projects []map[string]interface{}) []int {
	var ids []int
	for _, project := range projects {
		if id, ok := project["id"].(float64); ok {
			ids = append(ids, int(id))
		}
	}
	return ids
}
